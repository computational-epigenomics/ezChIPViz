## An R package for ChIP-seq analysis.

### Introduction
Typical workflow for ChIP-seq analysis includes manipulation of BigWig files, which provide input data for various analysis and plotting. Processing BigWig files is often slower in R or Python leading to time consuming processes. Current package utilizes [bwtool](https://github.com/CRG-Barcelona/bwtool) for BigWig file operations followed by data.table in R for rapid data summarization. 

## To Do
### Analysis
- [ ] Differentially bound peaks
- [ ] Peak summarization
- [ ] RNA and ChIP correlation
- [ ] RNA-Pol2 pausing Index estimation
- [ ] Super enhancer detection

### Visualization
- [x] Heatmap
- [x] Homer annotation pie chart
- [x] Profile plot
- [ ] Track plot


